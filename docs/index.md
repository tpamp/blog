---
hide:
  - footer
---

# Bienvenue sur Touche Pas à Ma Prod

Un blog créé par un SRE qui écrit des articles sur ce qu'il apprend

## Articles

- [K8sgpt : La baguette magique qui résout les problèmes de configuration Kubernetes](kubernetes/3-k8sgpt.md)
- [Comment j'expose un port local de ma machine dans un cluster distant ?](kubernetes/2-expose-local-port-to-remote-pods.md)
- [Comment les "alias" DNS Kubernetes nous ont brain toute une journée ?](kubernetes/1-alias-dns-kubernetes.md)
- [HAProxy & Tarpit](haproxy/1-HAProxy-Tarpit.md)

## Follow me 

- Twitter : [@sreguys](https://twitter.com/sreguys)
- Linkedin : [matthis-holleville](https://www.linkedin.com/in/matthis-holleville/)
- Github : [matthisholleville](https://github.com/matthisholleville)
