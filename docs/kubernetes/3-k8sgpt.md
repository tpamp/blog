# K8sgpt : La baguette magique qui résout les problèmes de configuration Kubernetes

![](https://github.com/k8sgpt-ai/k8sgpt/blob/main/images/banner-black.png?raw=true)

## Context

Vous êtes passionné par Kubernetes et vous souhaitez découvrir un outil innovant pour l'analyse de vos clusters ? `k8sgpt` est là pour vous aider !

### L'idée

Depuis la démocratisation de ChatGPT, les idées d'outils/business ne cessent d'inonder nos fils Twitter et les tendances Github. Un de ces outils a particulièrement attiré mon attention car il a été développé autour de la démarche SRE. Cet outil, c'est [k8sgpt](https://github.com/k8sgpt-ai). Il a été développé pour aider les SRE, les DevOps, etc., dans le diagnostic et la résolution des problèmes sur Kubernetes.

## Comment ça marche ?

`k8sgpt` a été construit sous forme de CLI à installer sur n'importe quel OS. Il permet d'appeler des analyseurs d'objets Kubernetes (Pod, Service, PVC, HPA, etc.). Ces analyseurs vont lire les ressources déployées sur votre cluster et vérifier que vos objets n'ont aucun problème de configuration.

### Quel est le rôle de l'IA dans cette histoire ?

`k8sgpt` dispose de deux modes (que je vous présenterai plus en détail dans la section suivante) :

1. Le mode `analyze` classique qui va retourner toutes les erreurs de configuration sans vous indiquer comment les régler.
2. Le mode `explain` qui va envoyer les erreurs à ChatGPT et demander de retourner une solution.

!!! note
    Actuellement, seul ChatGPT est disponible en tant que backend IA, mais nous prévoyons d'en intégrer d'autres dans les semaines à venir.

### Et la vie privée dans tout ça ?

Effectivement, c'est un point important à ne pas négliger. Comme l'a très bien expliqué [@alex.jones](https://twitter.com/AlexJonesax) dans cette [issue](https://github.com/k8sgpt-ai/k8sgpt/issues/139), il est prévu d'anonymiser les données envoyées à OpenAI (et autres backends).

## Comment l'utiliser ?

Avant tout, il vous faudra télécharger le binaire en fonction de votre OS. Je vous remets le lien du [dépôt](https://github.com/k8sgpt-ai) pour télécharger le bon binaire.


1- Génération de la configuration

```bash
$ k8sgpt generate
```
!!! note
    Cette étape est optionnelle si vous disposez déjà d'une clé d'API OpenAI.

2- Authentification

```bash
$ k8sgpt auth
```

!!! note
    Vous pouvez automatiquement ajouter votre clé en utilisant l'argument `--password $MY_OPENAI_KEY` à la commande.

3- Liste des analyseurs

```bash
$ k8sgpt filters list
Active: 
> ReplicaSet
> PersistentVolumeClaim
> Service
> Ingress
> Pod
Unused: 
> HorizontalPodAutoScaler
> PodDisruptionBudget
```

!!! note
    `k8sgpt` n'active pas tous les analyseurs par défaut. Cette décision s'explique par le fait de ne pas alourdir l'analyse.

    Nous prévoyons d'ajouter de nouveaux analyzers dans les semaines à venir. N'hésitez pas à contribuer.


4- Activation/Désactivation d'un analyseur (Optionnel)

```bash
$ k8sgpt filters add HorizontalPodAutoScaler
Filter HorizontalPodAutoScaler added
$ k8sgpt filters remove Pod
Filter(s) Pod removed
```

5- Lancement de l'analyse

```bash
$ k8sgpt analyze
0 test-ci-new-repo/fake-hpa-5()
- Error: HorizontalPodAutoscaler uses Service as ScaleTargetRef which does not possible option.
1 test-ci-new-repo/fake-hpa()
- Error: HorizontalPodAutoscaler uses StatefulSet/fake-deployment as ScaleTargetRef which does not exist.
```
!!! note
    Il est possible d'ajouter de nombreuses options à l'analyse. Pour récupérer l'ensemble des options, vous pouvez exécuter `k8sgpt analyze --help`

6- Lancement de l'analyse avec l'IA

```bash
$ k8sgpt analyze --explain
0 test-ci-new-repo/fake-hpa-5()
- Error: HorizontalPodAutoscaler uses Service as ScaleTargetRef which does not possible option.
The HorizontalPodAutoscaler is trying to use a Service as the target for scaling, but this is not allowed. To solve this, change the ScaleTargetRef to a Deployment or ReplicaSet.
1 test-ci-new-repo/fake-hpa()
- Error: HorizontalPodAutoscaler uses StatefulSet/fake-deployment as ScaleTargetRef which does not exist.
The HorizontalPodAutoscaler is set to use a StatefulSet or Deployment called "fake-deployment" as a reference for scaling, but that target does not exist.
To fix this error, update the HorizontalPodAutoscaler to use a valid target such as a deployed StatefulSet or Deployment.
2 test-ci-new-repo/fake-hpa-2()
- Error: HorizontalPodAutoscaler uses ReplicationController/fake-deployment as ScaleTargetRef which does not exist.
The Kubernetes error message means that the HorizontalPodAutoscaler is referring to a ScaleTargetRef called "fake-deployment" in the ReplicationController, but it cannot find it. 
```

## Conclusion

`k8sgpt` est un outil précieux pour les SRE, platform-engineer et devops, car il vous permet de comprendre rapidement ce qui se passe dans votre cluster sans avoir besoin de passer en revue de nombreux journaux et outils différents pour trouver la cause première d'un problème.

Essayez [k8sgpt](https://github.com/k8sgpt-ai) dès maintenant et n'hésitez pas à contribuer si le projet vous intéresse !
